#!/bin/python3
"""
How to use:
    python3 mra2csv.py MRA_FOLDER OUTPUT_CSV_NAME

By: Felix Leger (aka @barfood#4348) + Porkchop Express
    https://felixleger.com            https://misteraddons.com
Date: 20210202
"""

import os
import sys
import glob
import tqdm
from bs4 import BeautifulSoup as bs
import xml.etree.ElementTree as et
import pandas as pd
content = []

if len(sys.argv) != 3:
    print("How to use:\n\tpython3 extractor.py MRA_FOLDER OUTPUT_CSV_NAME")
    sys.exit(1)


# Loop over mra folder
MRA_FOLDER = sys.argv[1]
OUTPUT_CSV_NAME = sys.argv[2]
MRA_LIST = glob.glob(os.path.join(MRA_FOLDER, "*.mra"))
DATAFRAME = pd.DataFrame()
for mra in tqdm.tqdm(MRA_LIST, desc="Generating csv"):
    # Read the XML file
    info_dict = {}
    info_dict["mra_filename"] = os.path.basename(mra)
    with open(mra, "r") as file:

        content = file.readlines()
        content = "".join(content)

    # Parse xml
    bs_content = bs(content, "lxml")

    # Start extracting information here! ----------------------------------

    # Handle fields that necessitate exceptions:
    # (Add escape character to newlines (\n -> \\n), otherwise will split lines in csv)
    FIELDS_VERY_EXCEPTIONAL = ["rom", "switches"]
    FIELDS_NEED_SPLITTING = ["category", "manufacturer", "series"]
    FIELDS_SOMEWHAT_EXCEPTIONAL = ["buttons", "about", "nvram", "display", "mameinfo"]
    EXCEPTION_FIELDS = FIELDS_VERY_EXCEPTIONAL + FIELDS_SOMEWHAT_EXCEPTIONAL + FIELDS_NEED_SPLITTING

    for field in FIELDS_VERY_EXCEPTIONAL:
        if field == "rom":
            for tag in bs_content.find_all(field):
                info_dict["{}_{}".format(field, tag.attrs['index'])] = [str(tag).replace("\n", "\\n")]
        elif field == "switches":
            info_dict["{}".format(field)] = [str(bs_content.find(field)).replace("\n", "\\n")]

    for field in FIELDS_NEED_SPLITTING:
        info_dict[field] = " / ".join([i.text.replace("&", "&amp;") for i in bs_content.find_all(field)])

    for field in FIELDS_SOMEWHAT_EXCEPTIONAL:
        if field == "buttons":
            if bs_content.find(field) is not None:
                # buttons was renamed "button_names" in new gen of mras.
                button_fields = bs_content.find_all(field)
                for bf in button_fields:
                    if bf.has_attr("names"):
                        info_dict["button_names"] = [str(bf).replace("\n", "\\n")]
                    else:
                        info_dict["buttons"] = bf.text
        else:
            if bs_content.find(field) is not None:
                info_dict[field] = [str(bs_content.find(field)).replace("\n", "\\n").replace("&", "&amp;")]


    # Get all the other fields
    try:
        xtree = et.fromstring(content)
    except Exception as e:
        print("Error reading", mra)
        print(e)
        sys.exit(1)

    for node in xtree:
        if node.tag in EXCEPTION_FIELDS:
            # Skip fields we handle manually, we did them earlier
            continue
        if node.text is not None:
            info_dict[node.tag] = [node.text.replace("\n", "\\n").replace("&", "&amp;")]
        else:
            info_dict[node.tag] = None

    DATAFRAME = DATAFRAME.append(pd.DataFrame(info_dict, index=None), ignore_index=True, sort=True)

# Cleanup before export
DATAFRAME.replace(to_replace=[None, 'None'], value='', inplace=True)

# Sort columns
sorted_columns = ['mra_filename',
'name',
'region',
'homebrew',
'bootleg',
'version',
'alternative',
'platform',
'series',
'year',
'manufacturer',
'category',
'linebreak1',
'setname',
'parent',
'mameversion',
'rbf',
'about',
'linebreak2',
'resolution',
'rotation',
'flip',
'linebreak3',
'players',
'joystick',
'special_controls',
'buttons',
'button_names',
'linebreak4',
'switches',
'mameinfo',
'linebreak5',
'rom_1',
'rom_0',
'rom_2',
'rom_3',
'rom_4',
'linebreak6',
'nvram',
'linebreak7',
'remark',
'linebreak8',
'mratimestamp']
DATAFRAME = DATAFRAME.reindex(columns=sorted_columns)

# Export to file
DATAFRAME.to_csv(OUTPUT_CSV_NAME, index=False, na_rep='')
print("Generated", OUTPUT_CSV_NAME)
