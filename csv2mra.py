#!/bin/python3
# 20210425 Written by Felix Leger (aka @barfood#4348), with guidance from Porkchop Express.
# https://felixleger.com || https://misteraddons.com

"""
How to use:
    python3 csv2mra.py ARCADE_METADATA_FILE(.csv)

This will generate an "output_mras/" folder with 1 file per line in your ARCADE_METADATA_FILE.
"""

import os
import sys
import tqdm
import datetime
import numpy as np
import pandas as pd

if len(sys.argv) != 2:
    print("Please call the script as follows")
    print("python3 csv2mra.py ARCADE_METADATA_FILE(.csv)")
    sys.exit(1)

ARCADE_METADATA_CSV = sys.argv[1]
df = pd.read_csv(ARCADE_METADATA_CSV, na_values="", dtype=str)
df = df.replace(np.nan, '', regex=True)
MRA_NAME_COLUMN = "mra_filename"

if "mratimestamp" in df.columns:
    # Drop mratimestamp if it's in the csv, we will create it with this script
    del df["mratimestamp"]
TIMESTAMP = datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")

# Create output directory
OUTPUT_DIR = "output_mras"
os.makedirs(OUTPUT_DIR, exist_ok=True)

############### MAIN LOOP #########################
for _, game in tqdm.tqdm(df.iterrows(), desc="Generating mras", total=df.shape[0]):

    # Alternate mras are created in subfolders
    if len(game.alternative) > 0:
        alternate_folder = os.path.join(OUTPUT_DIR, "_alternatives", "_"+game.alternative.replace("&amp;", "&"))
        os.makedirs(alternate_folder, exist_ok=True)
        mra_filename = os.path.join(alternate_folder, game[MRA_NAME_COLUMN])
    else:
        mra_filename = os.path.join(OUTPUT_DIR, game[MRA_NAME_COLUMN])

    with open(mra_filename.replace("&amp;", "&"), 'w') as f:  # Write in file as utf-8
        f.write("<misterromdescription>\n")
        for key in game.keys():
            if key == MRA_NAME_COLUMN:
                # We don't want to include the MRA name as a field in the MRA itself.
                continue
            elif "linebreak" in key:
                # Any column that contains "linebreak" in its column name will be skipped and replaced by a
                # new line in the mra instead. (Remember, column names must be unique, so they will be
                # linebreak1, linebreak2, etc.)
                f.write("\n")
                continue

            # Indent
            f.write("\t")

            if len(game[key]) > 0 and game[key][0] == "<":
                # Special case, some fields contain xml code inside them (they are complete without us
                # handling writing the xml tag around the value)
                    f.write(game[key].replace("\\n", "\n\t\t") + "\n")
            else:
                if key in ["manufacturer", "series"]:
                    for i, val in enumerate(game[key].split(" / ")):
                        f.write("<{}>{}</{}>".format(key, val, key) + "\n")
                        if i < len(game[key].split(" / ")) - 1:
                            f.write("\t")
                else:
                    f.write("<{}>{}</{}>\n".format(key, game[key], key))

        # Insert mratimestamp (all mras produced in same batch have the same timestamp)
        f.write("\t<mratimestamp>{}</mratimestamp>\n".format(TIMESTAMP))
        f.write("</misterromdescription>\n")
############### END MAIN LOOP ##########################

print("MRAs have been generated in folder {}/".format(OUTPUT_DIR))
